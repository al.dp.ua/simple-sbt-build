FROM openjdk:8

COPY target/scala-2.11/sbt-sample-app_2.11-1.0.jar sbt-sample-app_2.11-1.0.jar

ENTRYPOINT [java]
CMD ["-jar", "sbt-sample-app_2.11-1.0.jar"]