# Sample Scala/SBT application

Simple SCALA application. Pipeline consist of three stages:

1. test (which runs auto-tests and coverage in project with sbt packager)
2. build (which compile and prepare .jar package)
3. deploy (which prepare dockerimage and push it to registry)

## Prerequisites
You will need to have **Scala** and **sbt** installed to run the project.

## Run tests
Execute `sbt test` in the project root directory.

## Measure test coverage
Execute `sbt clean coverage test coverageReport`.
